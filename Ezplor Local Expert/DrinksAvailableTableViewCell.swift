//
//  DrinksAvailableTableViewCell.swift
//  Ezplor Local Expert
//
//  Created by umit on 15/12/2017.
//  Copyright © 2017 Ezplor. All rights reserved.
//

import UIKit

class DrinksAvailableTableViewCell: UITableViewCell {

    @IBOutlet weak var radiNonAlcholic: RadioButton!
    @IBOutlet weak var radioAlcholic: RadioButton!
    @IBOutlet weak var radioNonAlcExtra: RadioButton!
    @IBOutlet weak var radioAlcExtra: RadioButton!
    @IBOutlet weak var radioOutsideAllowed: RadioButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
