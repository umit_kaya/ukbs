//
//  CustomPopupViewController.swift
//  Ezplor Local Expert
//
//  Created by umit on 30/09/2017.
//  Copyright © 2017 Ezplor. All rights reserved.
//

import UIKit
protocol CustomPopupViewControllerDelegate: class {
    func didFinishTask(sender: CustomPopupViewController)
}
extension Notification.Name {
    static let countrySelected = Notification.Name(rawValue: "countrySelected")
    static let citySelected = Notification.Name(rawValue: "citySelected")
}

class CustomPopupViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var PickButton: UIButton!
    @IBOutlet
    var tableView: UITableView!
    var itemsArray: [String] = []
    
    var popupType = ""
    var isACity: Bool = false
    var inCountry = ""
    
    var selectedDataArray: [String] = []
    var selectedCells = [NSIndexPath]()
    weak var delegate:CustomPopupViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.7)
        self.showAnimate()
        
        PickButton.isHidden = CustomPopupHidePickupButton(type: popupType)
        
        if let path = Bundle.main.path(forResource: "PropertyList", ofType: "plist") {
            
            if let propertiesDictionary = NSDictionary(contentsOfFile: path) as? [String: Any] {
                if !self.inCountry.isEmpty {
                    let properties = isACity ? propertiesDictionary[CitiesInCountry(country: inCountry)] as! NSArray : propertiesDictionary[popupType] as! NSArray
                    for value in properties {
                        itemsArray.append(value as! String)
                    }
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return itemsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as? LanTableViewCell
        cell?.Title.text = itemsArray[indexPath.row]
        cell?.accessoryType = selectedCells.contains(indexPath as NSIndexPath) ? .checkmark : .none
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedCell = tableView.cellForRow(at: indexPath as IndexPath)
        
        selectedCells.append(indexPath as NSIndexPath)
        
        if selectedCell?.accessoryType == .checkmark {
            
            selectedCell?.accessoryType = .none
            selectedDataArray = selectedDataArray.filter{$0 != self.itemsArray[indexPath.row]}
            selectedCells = selectedCells.filter {$0 as IndexPath != indexPath}
            
        } else {
            
            selectedDataArray.append(self.itemsArray[indexPath.row])
            selectedCell?.accessoryType = .checkmark
            CustomPopupMultipleSelectionAllowed(type: popupType) ? nil : removeAnimate()
        }
        
        let popupVC = LocationViewController()
        popupVC.state = (selectedDataArray.first)!
//        self.delegate?.didFinishTask(sender: self)
        if self.isACity {
            guard let citySelected = selectedDataArray.first else {
                return;
            }
            NotificationCenter.default.post(name: .citySelected, object: nil, userInfo: ["citySelected": citySelected])
        } else {
            guard let countrySelected = selectedDataArray.first else {
                return;
            }
            self.inCountry = countrySelected
            NotificationCenter.default.post(name: .countrySelected, object: nil, userInfo: ["countrySelected": countrySelected])
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func cancel(_ sender: Any) {
        self.removeAnimate()
    }
    @IBAction func pick(_ sender: Any) {
        self.removeAnimate()
    }
    
    func showAnimate()
    {
        self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0.0;
        UIView.animate(withDuration: 0.25, animations: {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        });
    }
    
    func removeAnimate()
    {
        UIView.animate(withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view.alpha = 0.0;
        }, completion:{(finished : Bool)  in
            if (finished)
            {
                self.view.removeFromSuperview()
            }
        });
    }
}
