//
//  ActionButton.swift
//  Ezplor Local Expert
//
//  Created by umit on 30/09/2017.
//  Copyright © 2017 Ezplor. All rights reserved.
//

import UIKit

class ActionButton: UIButton {
    
    let ezplorColors = EzplorColors()
    
    let buttonFont = "System"
    let buttonWidth: CGFloat = UIScreen.main.bounds.size.width
    let buttonHeight: CGFloat = 60

    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.layer.cornerRadius = 5.0
        self.clipsToBounds = true
        self.layer.borderWidth = 1.0
        self.layer.borderColor = ezplorColors.actionButtonBorderColor.cgColor
        self.layer.backgroundColor = ezplorColors.actionButtonBackgroundColor.cgColor
        
        self.setTitleColor(ezplorColors.actionButtonTitleColor, for: .normal)
        self.titleLabel?.font = UIFont(name: buttonFont, size: 25)
        self.titleLabel?.adjustsFontSizeToFitWidth = true
        self.setTitle(self.titleLabel?.text?.capitalized, for: .normal)
    }

}
