//
//  LoginViewController.swift
//  Ezplor Local Expert
//
//  Created by Soheil Nikbin on 07/08/2017.
//  Copyright © 2017 Ezplor. All rights reserved.
//

import UIKit
import Foundation
import XLPagerTabStrip
import JVFloatLabeledTextField
import UIColor_Hex_Swift

class LoginViewController: UIViewController, IndicatorInfoProvider, UITextFieldDelegate{
    
    
    @IBOutlet weak var emailAddress: JVFloatLabeledTextField!
    @IBOutlet weak var password: JVFloatLabeledTextField!
    
    @IBOutlet weak var backgroundScrollView: UIScrollView!
    @IBOutlet weak var LoginButton: UIButton!
    
    var ActiveOpenKeyboard: Bool = true
    var KeyboardIsOn: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        emailAddress.delegate = self
        emailAddress.tag = 0
        password.delegate = self
        password.tag = 1
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        LoginButton.addTarget(self, action: #selector(thumbsUpButtonPressed), for: .touchUpInside)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        // Try to find next responder
        if let nextField = textField.superview?.viewWithTag(textField.tag + 1) as? UITextField {
            nextField.becomeFirstResponder()
        } else {
            // Not found, so remove keyboard.
            textField.resignFirstResponder()
            ActiveOpenKeyboard = true
            self.view.endEditing(true)
        }
        // Do not add a line break
        return false
    }
    
    func keyboardWillShow(notification:NSNotification) {
        adjustingHeight(show: true, notification: notification)
        KeyboardIsOn = true
    }
    
    func keyboardWillHide(notification:NSNotification) {
        KeyboardIsOn = false
        ActiveOpenKeyboard = true
        adjustingHeight(show: false, notification: notification)
        
    }
    
    func adjustingHeight(show:Bool, notification:NSNotification) {
        
        if(ActiveOpenKeyboard == true){
            ActiveOpenKeyboard = false
            
            let userInfo = notification.userInfo!
            
            let keyboardFrame = (userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
            
            print("keyboardFrame = \(backgroundScrollView.contentInset.bottom)")
            let changeInHeight = (keyboardFrame.height + 40) * (show ? 1 : -1)
            
            if show == false{
                backgroundScrollView.contentInset.bottom = 0
                backgroundScrollView.scrollIndicatorInsets.bottom = 0
            }else{
                backgroundScrollView.contentInset.bottom += changeInHeight
                
                backgroundScrollView.scrollIndicatorInsets.bottom += changeInHeight
            }
            
            
            print("after keyboardFrame = \(backgroundScrollView.contentInset.bottom)")
        }
    }
    
    func textFieldShouldReturnScroll(textField: JVFloatLabeledTextField) -> Bool {
        self.view.endEditing(false)
        return true
    }
    
    func thumbsUpButtonPressed() {
        
        let VC1 = self.storyboard!.instantiateViewController(withIdentifier: "home") as! SWRevealViewController
        self.present(VC1, animated:true, completion: nil)
        
        //Comment for testing...
        
//        if !(emailAddress.text?.isEmpty)! && !(password.text?.isEmpty)! {
//
//            let apiServices = APIServices.shared
//            apiServices.login(email: emailAddress.text!, password: password.text!) { (success, errorMessage, response) in
//
//                print("\(response)")
//                if success == true {
//
//                    let VC1 = self.storyboard!.instantiateViewController(withIdentifier: "home") as! SWRevealViewController
//                    self.present(VC1, animated:true, completion: nil)
//                }
//                else
//                {
//                    //API has an issue now..
//                    print(response.description)
//                }
//            }
//        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: "LOGIN")
    }
}
