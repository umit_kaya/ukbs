//
//  CreateItenaryDescBoxTableViewCell.swift
//  Ezplor Local Expert
//
//  Created by umit on 15/12/2017.
//  Copyright © 2017 Ezplor. All rights reserved.
//

import UIKit

class CreateItenaryDescBoxTableViewCell: UITableViewCell {

    @IBOutlet weak var addItem: UIButton!
    @IBOutlet weak var cellDescLabel: UILabel!
    @IBOutlet weak var descTextBox: UITextView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
