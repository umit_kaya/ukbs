//
//  PaymentViewController.swift
//  Ezplor Local Expert
//
//  Created by Soheil Nikbin on 09/08/2017.
//  Copyright © 2017 Ezplor. All rights reserved.
//

import UIKit

class PaymentViewController: UIViewController {
    
    @IBOutlet weak var RadioButtonPaypal: RadioButton!
    @IBOutlet weak var RadioButtonBankTransfer: RadioButton!
    @IBOutlet weak var menuButton: UIBarButtonItem!
    @IBOutlet weak var PaypalEmailTextView: UIView!
    @IBOutlet weak var PaymentMethodHeightConstraint: NSLayoutConstraint!
    
    var isBankTransferChecked = true
    var isPaypalChecked = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
    }
    
    func setupUI() {
        if revealViewController() != nil {
            menuButton.target = revealViewController()
            menuButton.action = #selector(SWRevealViewController.revealToggle(_:))
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        
        RadioButtonBankTransfer.addTarget(self, action: #selector(RadioButtonPressed), for: .touchUpInside)
        RadioButtonPaypal.addTarget(self, action: #selector(RadioButtonPressed), for: .touchUpInside)
        RadioButtonBankTransfer.isSelected = true
        isPaypalTransfer(isPaypal: false)
    }
    
    func RadioButtonPressed() {
        if isBankTransferChecked {
            isBankTransferChecked = false
            RadioButtonBankTransfer.isSelected = false
            RadioButtonPaypal.isSelected = true
            isPaypalChecked = true
            isPaypalTransfer(isPaypal: true)
        } else {
            isBankTransferChecked = true
            RadioButtonBankTransfer.isSelected = true
            RadioButtonPaypal.isSelected = false
            isPaypalChecked = false
            isPaypalTransfer(isPaypal: false)
        }
    }
    
    func isPaypalTransfer(isPaypal: Bool) {
        if isPaypal {
            UIView.animate(withDuration: 0.5, animations: {
                self.PaymentMethodHeightConstraint.constant = 180
                self.PaypalEmailTextView.isHidden = false
            })
        } else {
            UIView.animate(withDuration: 0.5, animations: {
                self.PaymentMethodHeightConstraint.constant = 130
                self.PaypalEmailTextView.isHidden = true
            })
        }
    }
    
}

extension PaymentViewController {
    @IBAction func saveBarButtonItemTapped(_ sender: Any) {
    }
}
