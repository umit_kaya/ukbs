//
//  LoginApi.swift
//  Ezplor Local Expert
//
//  Created by umit on 16/10/2017.
//  Copyright © 2017 Ezplor. All rights reserved.
//

import Foundation
import Alamofire

let token = "Api_Client_Token_Client_App_60"
//explore_admin
//Exploreadmin123$$

func loginUserAccount(username: String, password: String) {
    Alamofire.request(
        URL(string: "http://dev.ezplor.com/user/restful_login")!,
        method: .post,
        parameters: ["name": username, "pass": password, "token": token])
        //headers: ["Authorization": "Basic xxx"]
        .validate()
        .responseString { (response) -> Void in
            
            guard response.result.isSuccess else {
                assert(true, response.result.error as! String)
                return
            }
            
            guard let responseValue = response.result.value else {
                assert(true, response.result.value!)
                return
            }
            
            let data = responseValue.data(using: String.Encoding.utf8, allowLossyConversion: false)!
            
            do {
                
                let json = try JSONSerialization.jsonObject(with: data, options: []) as! [String: AnyObject]
                for (key, value) in json {
                    print(key, value)
                }
            } catch let error as NSError {
                assert(true, error.domain)
            }
    }
}
