//
//  NewExperienceViewController.swift
//  Ezplor Local Expert
//
//  Created by Soheil Nikbin on 09/08/2017.
//  Copyright © 2017 Ezplor. All rights reserved.
//

import UIKit

class NewExperienceViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var progressContainer: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var menuButton: UIBarButtonItem!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        
        tableView.register(UINib(nibName: "CreateInfoTableViewCell", bundle: nil), forCellReuseIdentifier: "createInfoCell")
        tableView.register(UINib(nibName: "CreateInfoDescTableViewCell", bundle: nil), forCellReuseIdentifier: "infoDescCell")
        
        self.tableView.dataSource = self
        self.tableView.delegate = self
    }
    
    func setupUI() {
        if revealViewController() != nil {
            menuButton.target = revealViewController()
            menuButton.action = #selector(SWRevealViewController.revealToggle(_:))
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        switch indexPath.row {
        case 0:
            return 385
        default:
            return 190
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "createInfoCell", for: indexPath) as? CreateInfoTableViewCell
            
            cell?.selectionStyle = .none
            
            return cell!
        default:
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "infoDescCell", for: indexPath) as? CreateInfoDescTableViewCell
            
            cell?.selectionStyle = .none
            
            return cell!
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    
}

extension NewExperienceViewController {
    @IBAction func itenaryNextBarButtonItemTapped(_ sender: Any) {
        guard let vc = UIStoryboard(name:"NewExperience", bundle:nil).instantiateViewController(withIdentifier: "NewExperiencesItenaryViewController") as? NewExperiencesItenaryViewController else {
            assert(false, "Could not instantiate view controller with identifier of type ContactViewController")
            return
        }
        self.navigationController?.pushViewController(vc, animated:true)
    }
}
