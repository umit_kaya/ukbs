//
//  NewExperiencesScheduleViewController.swift
//  Ezplor Local Expert
//
//  Created by umit on 10/10/2017.
//  Copyright © 2017 Ezplor. All rights reserved.
//

import UIKit
import XLPagerTabStrip

class NewExperiencesScheduleViewController: UIViewController, IndicatorInfoProvider {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: " Schedule ")
    }

}

extension NewExperiencesScheduleViewController {
    @IBAction func backBarButtonItemTapped(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    @IBAction func doneBarButtonItemTapped(_ sender: Any) {
    }
}
