//
//  PreviewPlanCardTableViewCell.swift
//  Ezplor Local Expert
//
//  Created by umit on 08/10/2017.
//  Copyright © 2017 Ezplor. All rights reserved.
//

import UIKit

class PreviewPlanCardTableViewCell: UITableViewCell {

    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
