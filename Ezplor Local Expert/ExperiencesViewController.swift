//
//  ExperiencesViewController.swift
//  Ezplor Local Expert
//
//  Created by Soheil Nikbin on 09/08/2017.
//  Copyright © 2017 Ezplor. All rights reserved.
//

import UIKit

class ExperiencesViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var ExperiencesSearchBar: UISearchBar!
    @IBOutlet weak var menuButton: UIBarButtonItem!
    @IBOutlet weak var SelectStatusView: CardView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        
        tableView.register(UINib(nibName: "ExperiencesListTableViewCell", bundle: nil), forCellReuseIdentifier: "experiencesCell")
        
        self.tableView.dataSource = self
        self.tableView.delegate = self
    }
    
    func setupUI() {
        if revealViewController() != nil {
            menuButton.target = revealViewController()
            menuButton.action = #selector(SWRevealViewController.revealToggle(_:))
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        
        let tapStatusView = UITapGestureRecognizer(target: self, action: #selector(showPopup))
        tapStatusView.delegate = self as? UIGestureRecognizerDelegate
        SelectStatusView.addGestureRecognizer(tapStatusView)
    }
    
    func showPopup(sender: UITapGestureRecognizer? = nil) {
        if let popupType: String = PopupTypeByTag(tagNo: (sender?.view?.tag)!) {
            let CustomPopUp = UIStoryboard(name: "CustomPopupStoryboard", bundle: nil).instantiateViewController(withIdentifier: "custom_popup") as! CustomPopupViewController
            CustomPopUp.popupType = popupType
            self.addChildViewController(CustomPopUp)
            CustomPopUp.view.frame = self.view.frame
            self.view.addSubview(CustomPopUp.view)
            CustomPopUp.didMove(toParentViewController: self)
        } else {
            assert(false, "Add TAG for the view!")
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 9
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 180
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "experiencesCell", for: indexPath) as? ExperiencesListTableViewCell
        
        cell?.selectionStyle = .none
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let editAction = UITableViewRowAction(style: .normal, title: "Edit", handler: { (action, indexPath) in
            
        })
        
        
        let previewAction = UITableViewRowAction(style: .normal, title: "Preview", handler: { (action, indexPath) in
            self.previewTheCard()
        })
        
        previewAction.backgroundColor = UIColor.lightGray
        
        
        return [editAction, previewAction]
    }
    
    func previewTheCard() {
        guard let vc = UIStoryboard(name:"Experiences", bundle:nil).instantiateViewController(withIdentifier: "ExperiencePreviewViewController") as? ExperiencePreviewViewController else {
            assert(false, "Could not instantiate view controller with identifier of type ContactViewController")
            return
        }
        self.navigationController?.pushViewController(vc, animated:true)
    }
    
}

extension ExperiencesViewController {
    @IBAction func newExperience(_ sender: Any) {
        guard let vc = UIStoryboard(name:"NewExperience", bundle:nil).instantiateViewController(withIdentifier: "NewExperienceViewController") as? NewExperienceViewController else {
            assert(false, "Could not instantiate view controller with identifier of type ContactViewController")
            return
        }
        self.navigationController?.pushViewController(vc, animated:true)
    }
}
