//
//  PreviewProfileTableViewCell.swift
//  Ezplor Local Expert
//
//  Created by umit on 08/10/2017.
//  Copyright © 2017 Ezplor. All rights reserved.
//

import UIKit
import Cosmos

class PreviewProfileTableViewCell: UITableViewCell {

    @IBOutlet weak var ratingView: CosmosView!
    
    override func awakeFromNib() {
        super.awakeFromNib()

        ratingView.rating = 4.3
        ratingView.settings.fillMode = .precise
//        ratingView.settings.updateOnTouch = false
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
