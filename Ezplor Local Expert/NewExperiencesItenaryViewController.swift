//
//  NewExperiencesInfoViewController.swift
//  Ezplor Local Expert
//
//  Created by umit on 10/10/2017.
//  Copyright © 2017 Ezplor. All rights reserved.
//

import UIKit
import XLPagerTabStrip

class NewExperiencesItenaryViewController: UIViewController, IndicatorInfoProvider {
    
    @IBOutlet weak var tableView: UITableView!
    
    var isMealAvailableYes = false
    var isMealAvailableNo = false
    
    var alcholicSelected = false
    var alcholicExtraSelected = false
    var nonAlcholicExtraSelected = false
    var nonAlcholicSelected = false
    var outsideAllowedSelected = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
    }
    
    func setupUI() {
        tableView.register(UINib(nibName: "CreateItenaryDescBoxTableViewCell", bundle: nil), forCellReuseIdentifier: "itenaryDescBoxCell")
        tableView.register(UINib(nibName: "CreateInfoDescTableViewCell", bundle: nil), forCellReuseIdentifier: "infoDescCell")
        tableView.register(UINib(nibName: "CardTitleTableViewCell", bundle: nil), forCellReuseIdentifier: "titleLabelCell")
        tableView.register(UINib(nibName: "NewItenaryPickViewTableViewCell", bundle: nil), forCellReuseIdentifier: "itenaryPickerCell")
        tableView.register(UINib(nibName: "MealsAvailableTableViewCell", bundle: nil), forCellReuseIdentifier: "mealsAvailableCell")
        tableView.register(UINib(nibName: "DrinksAvailableTableViewCell", bundle: nil), forCellReuseIdentifier: "drinksAvailableCell")
        
        self.tableView.dataSource = self
        self.tableView.delegate = self
        
    }
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: " Itenary ")
    }
    
}

extension NewExperiencesItenaryViewController {
    @IBAction func backButtonTapped(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    @IBAction func nextBarButtonItemTapped(_ sender: Any) {
        guard let vc = UIStoryboard(name:"NewExperience", bundle:nil).instantiateViewController(withIdentifier: "NewExperiencesMeetingViewController") as? NewExperiencesMeetingViewController else {
            assert(false, "Could not instantiate view controller with identifier of type ContactViewController")
            return
        }
        self.navigationController?.pushViewController(vc, animated:true)
    }
}

extension NewExperiencesItenaryViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 11
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        switch indexPath.row {
        case 0:
            return 40
        case 1:
            return 90
        case 2:
            return 40
        case 3:
            return 90
        case 4:
            return 150
        case 5:
            return 90
        case 6:
            return 90
        case 7:
            return 150
        case 8:
            return 250
        case 9:
            return 90
        default:
            return 90
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "titleLabelCell", for: indexPath) as? CardTitleTableViewCell
            
            cell?.selectionStyle = .none
            
            cell?.titleLabel.text = "Experience Highlights"
            
            return cell!
        case 1:
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "itenaryDescBoxCell", for: indexPath) as? CreateItenaryDescBoxTableViewCell
            
            cell?.selectionStyle = .none
            
            
            cell?.cellDescLabel.text = "HIGHLIGHTS"
            cell?.addItem.addTarget(self, action: #selector(NewExperiencesItenaryViewController.addExperience(sender:)), for: .touchUpInside)

            
            return cell!
        case 2:
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "titleLabelCell", for: indexPath) as? CardTitleTableViewCell
            
            cell?.selectionStyle = .none
            
            cell?.titleLabel.text = "What is the plan?"
            
            return cell!
        case 3:
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "itenaryDescBoxCell", for: indexPath) as? CreateItenaryDescBoxTableViewCell
            
            cell?.selectionStyle = .none
            
            cell?.cellDescLabel.text = "STOP/ACTIVITY"
            cell?.addItem.addTarget(self, action: #selector(NewExperiencesItenaryViewController.addHighlights(sender:)), for: .touchUpInside)
            
            return cell!
        case 4:
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "infoDescCell", for: indexPath) as? CreateInfoDescTableViewCell
            
            cell?.selectionStyle = .none
            
            return cell!
        case 5:
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "itenaryPickerCell", for: indexPath) as? NewItenaryPickViewTableViewCell
            
            cell?.selectionStyle = .none
        
            cell?.pickerMainTitle.text = "PICKUP"
            cell?.pickerTitle.text = "Please Select"
 
            let tapPickup = UITapGestureRecognizer(target: self, action: #selector(addPickup))
            tapPickup.delegate = self as? UIGestureRecognizerDelegate
            cell?.pickerView.addGestureRecognizer(tapPickup)
            
            return cell!
        case 6:
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "itenaryPickerCell", for: indexPath) as? NewItenaryPickViewTableViewCell
            
            cell?.selectionStyle = .none
            
            cell?.pickerMainTitle.text = "TRANSPORT MODE"
            cell?.pickerTitle.text = "Please Select"
            
            let tapTransportation = UITapGestureRecognizer(target: self, action: #selector(addTransport))
            tapTransportation.delegate = self as? UIGestureRecognizerDelegate
            cell?.pickerView.addGestureRecognizer(tapTransportation)
            
            return cell!
        case 7:
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "mealsAvailableCell", for: indexPath) as? MealsAvailableTableViewCell
            
            cell?.selectionStyle = .none
            
            cell?.radioButtonYes.isSelected = false
            cell?.radioButtonNo.isSelected = false
            cell?.radioButtonVeggy.isSelected = false
            cell?.radiButtonNonveggy.isSelected = false
            
            cell?.radioButtonYes.addTarget(self, action: #selector(yesRadioPressed), for: .touchUpInside)
            cell?.radioButtonNo.addTarget(self, action: #selector(noRadioPressed), for: .touchUpInside)
            
            cell?.mealDetailsView.isHidden = true
            
            return cell!
        case 8:
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "drinksAvailableCell", for: indexPath) as? DrinksAvailableTableViewCell
            
            cell?.radiNonAlcholic.addTarget(self, action: #selector(nonAlcholicPressed), for: .touchUpInside)
            cell?.radioAlcholic.addTarget(self, action: #selector(alcholicPressed), for: .touchUpInside)
            cell?.radioAlcExtra.addTarget(self, action: #selector(alcholicExtraPressed), for: .touchUpInside)
            cell?.radioNonAlcExtra.addTarget(self, action: #selector(nonAlcholicExtraPressed), for: .touchUpInside)
            cell?.radioOutsideAllowed.addTarget(self, action: #selector(outsideAllowedPressed), for: .touchUpInside)
            
            cell?.selectionStyle = .none
            
            return cell!
        case 9:
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "itenaryPickerCell", for: indexPath) as? NewItenaryPickViewTableViewCell
            
            cell?.selectionStyle = .none
            
            cell?.pickerMainTitle.text = "INCLUDED"
            cell?.pickerTitle.text = "Please Select"
            
            let tapIncluded = UITapGestureRecognizer(target: self, action: #selector(addIncluded))
            tapIncluded.delegate = self as? UIGestureRecognizerDelegate
            cell?.pickerView.addGestureRecognizer(tapIncluded)
            
            return cell!
        default:
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "itenaryPickerCell", for: indexPath) as? NewItenaryPickViewTableViewCell
            
            cell?.selectionStyle = .none
            
            cell?.pickerMainTitle.text = "EXCLUDED"
            cell?.pickerTitle.text = "Please Select"
            
            let tapExcluded = UITapGestureRecognizer(target: self, action: #selector(addExcluded))
            tapExcluded.delegate = self as? UIGestureRecognizerDelegate
            cell?.pickerView.addGestureRecognizer(tapExcluded)
            
            return cell!
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    
    // # Tap/Click Handlings
    func yesRadioPressed() {
        let indexPath = IndexPath(row: 7, section: 0)
        let cell = tableView.cellForRow(at: indexPath) as! MealsAvailableTableViewCell
        
        if isMealAvailableYes {
            isMealAvailableYes = false
            isMealAvailableNo = false
            cell.radioButtonYes.isSelected = false
            cell.radioButtonNo.isSelected = false
        } else {
            isMealAvailableYes = true
            isMealAvailableNo = false
            cell.radioButtonYes.isSelected = true
            cell.radioButtonNo.isSelected = false
        }
    }
    
    func noRadioPressed() {
        let indexPath = IndexPath(row: 7, section: 0)
        let cell = tableView.cellForRow(at: indexPath) as! MealsAvailableTableViewCell
        
        if isMealAvailableNo {
            isMealAvailableYes = false
            isMealAvailableNo = false
            cell.radioButtonYes.isSelected = false
            cell.radioButtonNo.isSelected = false
        } else {
            isMealAvailableYes = false
            isMealAvailableNo = true
            cell.radioButtonYes.isSelected = false
            cell.radioButtonNo.isSelected = true
        }
    }
    
    func nonAlcholicPressed() {
        let indexPath = IndexPath(row: 8, section: 0)
        let drinksCell = tableView.cellForRow(at: indexPath) as! DrinksAvailableTableViewCell
        
        if nonAlcholicSelected {
            nonAlcholicSelected = false
            alcholicSelected = false
            alcholicExtraSelected = false
            nonAlcholicExtraSelected = false
            outsideAllowedSelected = false
            
            drinksCell.radiNonAlcholic.isSelected = false
            drinksCell.radioOutsideAllowed.isSelected = false
            drinksCell.radioNonAlcExtra.isSelected = false
            drinksCell.radioAlcExtra.isSelected = false
            drinksCell.radioAlcholic.isSelected = false
        } else {
            nonAlcholicSelected = true
            alcholicSelected = false
            alcholicExtraSelected = false
            nonAlcholicExtraSelected = false
            outsideAllowedSelected = false
            
            drinksCell.radiNonAlcholic.isSelected = true
            drinksCell.radioOutsideAllowed.isSelected = false
            drinksCell.radioNonAlcExtra.isSelected = false
            drinksCell.radioAlcExtra.isSelected = false
            drinksCell.radioAlcholic.isSelected = false
        }
    }
    
    func alcholicPressed() {
        let indexPath = IndexPath(row: 8, section: 0)
        let drinksCell = tableView.cellForRow(at: indexPath) as! DrinksAvailableTableViewCell
        
        if alcholicSelected {
            nonAlcholicSelected = false
            alcholicSelected = false
            alcholicExtraSelected = false
            nonAlcholicExtraSelected = false
            outsideAllowedSelected = false
            
            drinksCell.radiNonAlcholic.isSelected = false
            drinksCell.radioOutsideAllowed.isSelected = false
            drinksCell.radioNonAlcExtra.isSelected = false
            drinksCell.radioAlcExtra.isSelected = false
            drinksCell.radioAlcholic.isSelected = false
        } else {
            nonAlcholicSelected = false
            alcholicSelected = true
            alcholicExtraSelected = false
            nonAlcholicExtraSelected = false
            outsideAllowedSelected = false
            
            drinksCell.radiNonAlcholic.isSelected = false
            drinksCell.radioOutsideAllowed.isSelected = false
            drinksCell.radioNonAlcExtra.isSelected = false
            drinksCell.radioAlcExtra.isSelected = false
            drinksCell.radioAlcholic.isSelected = true
        }
    }
    
    func nonAlcholicExtraPressed() {
        let indexPath = IndexPath(row: 8, section: 0)
        let drinksCell = tableView.cellForRow(at: indexPath) as! DrinksAvailableTableViewCell
        
        if nonAlcholicExtraSelected {
            nonAlcholicSelected = false
            alcholicSelected = false
            alcholicExtraSelected = false
            nonAlcholicExtraSelected = false
            outsideAllowedSelected = false
            
            drinksCell.radiNonAlcholic.isSelected = false
            drinksCell.radioOutsideAllowed.isSelected = false
            drinksCell.radioNonAlcExtra.isSelected = false
            drinksCell.radioAlcExtra.isSelected = false
            drinksCell.radioAlcholic.isSelected = false
        } else {
            nonAlcholicSelected = false
            alcholicSelected = false
            alcholicExtraSelected = false
            nonAlcholicExtraSelected = true
            outsideAllowedSelected = false
            
            drinksCell.radiNonAlcholic.isSelected = false
            drinksCell.radioOutsideAllowed.isSelected = false
            drinksCell.radioNonAlcExtra.isSelected = true
            drinksCell.radioAlcExtra.isSelected = false
            drinksCell.radioAlcholic.isSelected = false
        }
    }
    
    func alcholicExtraPressed() {
        let indexPath = IndexPath(row: 8, section: 0)
        let drinksCell = tableView.cellForRow(at: indexPath) as! DrinksAvailableTableViewCell
        
        if alcholicExtraSelected {
            nonAlcholicSelected = false
            alcholicSelected = false
            alcholicExtraSelected = false
            nonAlcholicExtraSelected = false
            outsideAllowedSelected = false
            
            drinksCell.radiNonAlcholic.isSelected = false
            drinksCell.radioOutsideAllowed.isSelected = false
            drinksCell.radioNonAlcExtra.isSelected = false
            drinksCell.radioAlcExtra.isSelected = false
            drinksCell.radioAlcholic.isSelected = false
        } else {
            nonAlcholicSelected = false
            alcholicSelected = false
            alcholicExtraSelected = true
            nonAlcholicExtraSelected = false
            outsideAllowedSelected = false
            
            drinksCell.radiNonAlcholic.isSelected = false
            drinksCell.radioOutsideAllowed.isSelected = false
            drinksCell.radioNonAlcExtra.isSelected = false
            drinksCell.radioAlcExtra.isSelected = true
            drinksCell.radioAlcholic.isSelected = false
        }
    }
    
    func outsideAllowedPressed() {
        let indexPath = IndexPath(row: 8, section: 0)
        let drinksCell = tableView.cellForRow(at: indexPath) as! DrinksAvailableTableViewCell
        
        if outsideAllowedSelected {
            nonAlcholicSelected = false
            alcholicSelected = false
            alcholicExtraSelected = false
            nonAlcholicExtraSelected = false
            outsideAllowedSelected = false
            
            drinksCell.radiNonAlcholic.isSelected = false
            drinksCell.radioOutsideAllowed.isSelected = false
            drinksCell.radioNonAlcExtra.isSelected = false
            drinksCell.radioAlcExtra.isSelected = false
            drinksCell.radioAlcholic.isSelected = false
        } else {
            nonAlcholicSelected = false
            alcholicSelected = false
            alcholicExtraSelected = false
            nonAlcholicExtraSelected = false
            outsideAllowedSelected = true
            
            drinksCell.radiNonAlcholic.isSelected = false
            drinksCell.radioOutsideAllowed.isSelected = true
            drinksCell.radioNonAlcExtra.isSelected = false
            drinksCell.radioAlcExtra.isSelected = false
            drinksCell.radioAlcholic.isSelected = false
        }
    }
    
    func addIncluded(sender: UITapGestureRecognizer? = nil) {
    }
    
    func addExcluded(sender: UITapGestureRecognizer? = nil){
        
    }
    
    func addExperience(sender: UIButton){
    }
    
    func addHighlights(sender: UIButton){
    }
    
    func addPickup(sender: UITapGestureRecognizer? = nil) {
        // handling code
    }
    
    func addTransport(sender: UITapGestureRecognizer? = nil) {
        // handling code
    }
    
}
