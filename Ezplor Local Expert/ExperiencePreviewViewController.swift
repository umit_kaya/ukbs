//
//  ExperiencePreviewViewController.swift
//  Ezplor Local Expert
//
//  Created by umit on 07/10/2017.
//  Copyright © 2017 Ezplor. All rights reserved.
//

import UIKit


class ExperiencePreviewViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    var itemsArray: [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let path = Bundle.main.path(forResource: "PropertyList", ofType: "plist") {
            
            if let propertiesDictionary = NSDictionary(contentsOfFile: path) as? [String: Any] {
                let properties = propertiesDictionary["TourDetailKeys"] as! NSArray
                for value in properties {
                    itemsArray.append(value as! String)
                }
            }
        }
        
        tableView.register(UINib(nibName: "PreviewBriefTableViewCell", bundle: nil), forCellReuseIdentifier: "previewBriefCell")
        tableView.register(UINib(nibName: "PreviewProfileTableViewCell", bundle: nil), forCellReuseIdentifier: "profileCell")
        tableView.register(UINib(nibName: "PreviewPlanCardTableViewCell", bundle: nil), forCellReuseIdentifier: "previewTourDescCell")
        tableView.register(UINib(nibName: "PreviewTourDetailsTableViewCell", bundle: nil), forCellReuseIdentifier: "keyValueCell")
        tableView.register(UINib(nibName: "PreviewButtonTableViewCell", bundle: nil), forCellReuseIdentifier: "contactCell")

        self.tableView.dataSource = self
        self.tableView.delegate = self
        
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 150
        
       
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 14
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "previewBriefCell", for: indexPath) as? PreviewBriefTableViewCell
            
            cell?.selectionStyle = .none
            
            return cell!
        case 1:
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "profileCell", for: indexPath) as? PreviewProfileTableViewCell
            
            cell?.selectionStyle = .none
            
            return cell!
        case 2:
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "previewTourDescCell", for: indexPath) as? PreviewPlanCardTableViewCell
            
            cell?.selectionStyle = .none
            cell?.titleLabel.text = "What you can expect"
            cell?.descriptionLabel.text = "This is a short description about what you can expect. This is to test a longgger description compare to other lines."
            
            return cell!
        case 3:
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "previewTourDescCell", for: indexPath) as? PreviewPlanCardTableViewCell
            
            cell?.selectionStyle = .none
            cell?.titleLabel.text = "This is the plan"
            cell?.descriptionLabel.text = "This is a short description about what is the plan."
            
            return cell!
        case 13:
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "contactCell", for: indexPath) as? PreviewButtonTableViewCell
            
            cell?.selectionStyle = .none
            
            return cell!
        default:
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "keyValueCell", for: indexPath) as? PreviewTourDetailsTableViewCell
            
            cell?.selectionStyle = .none
            let itemIndex = indexPath.row - 4
            cell?.keysLabel.text = itemsArray[itemIndex]
            
            return cell!
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }

}

extension ExperiencePreviewViewController {
    @IBAction func tappedBackButton(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
    }
}
