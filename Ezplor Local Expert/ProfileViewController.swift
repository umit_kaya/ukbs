//
//  MapViewController.swift
//  SidebarMenu
//
//  Created by Simon Ng on 2/2/15.
//  Copyright (c) 2015 AppCoda. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController {
    @IBOutlet weak var menuButton:UIBarButtonItem!
    @IBOutlet weak var RadioButtonMale: RadioButton!
    @IBOutlet weak var ButtonMale: UIButton!

    @IBOutlet weak var RadioButtonFemale: RadioButton!
    @IBOutlet weak var ButtonFemale: UIButton!
    @IBOutlet weak var pickLanguage: CardView!
    @IBOutlet weak var pickCurrency: CardView!
    
    @IBOutlet weak var RadioButtonTermsAndConditions: RadioButton!
    @IBOutlet weak var RadioButtonCompanyAccount: RadioButton!
    @IBOutlet weak var CompanyAccountTextsView: UIView!
    
    var isMaleCheck = false
    var isFemaleCheck = false
    var isTermsAndContionsChecked = false
    var isCompanyAccountChecked = false
    
    @IBOutlet weak var CompanyAccountCardViewHeightConstraint: NSLayoutConstraint!
    override func viewDidLoad() {
        super.viewDidLoad()
    
        let tapLanguage = UITapGestureRecognizer(target: self, action: #selector(showPopup))
        tapLanguage.delegate = self as? UIGestureRecognizerDelegate
        pickLanguage.addGestureRecognizer(tapLanguage)
        
        let tapCurrency = UITapGestureRecognizer(target: self, action: #selector(showPopup))
        tapCurrency.delegate = self as? UIGestureRecognizerDelegate
        pickCurrency.addGestureRecognizer(tapCurrency)
        
        ButtonMale.addTarget(self, action: #selector(MaleButtonPressed), for: .touchUpInside)
        RadioButtonMale.addTarget(self, action: #selector(MaleButtonPressed), for: .touchUpInside)
        
        ButtonFemale.addTarget(self, action: #selector(FemaleButtonPressed), for: .touchUpInside)
        RadioButtonFemale.addTarget(self, action: #selector(FemaleButtonPressed), for: .touchUpInside)
        
        RadioButtonTermsAndConditions.addTarget(self, action: #selector(TermsAndConditionsButtonPressed), for: .touchUpInside)
        RadioButtonCompanyAccount.addTarget(self, action: #selector(CompanyAccountButtonPressed), for: .touchUpInside)
        isCompanyAccount(isCompany: false)
        
        if revealViewController() != nil {
            menuButton.target = revealViewController()
            menuButton.action = #selector(SWRevealViewController.revealToggle(_:))
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func showPopup(sender: UITapGestureRecognizer? = nil) {
        if let popupType: String = PopupTypeByTag(tagNo: (sender?.view?.tag)!) {
            let CustomPopUp = UIStoryboard(name: "CustomPopupStoryboard", bundle: nil).instantiateViewController(withIdentifier: "custom_popup") as! CustomPopupViewController
            CustomPopUp.popupType = popupType
            self.addChildViewController(CustomPopUp)
            CustomPopUp.view.frame = self.view.frame
            self.view.addSubview(CustomPopUp.view)
            CustomPopUp.didMove(toParentViewController: self)
        } else {
            assert(false, "Add TAG for the view!")
        }
    }
    
    func MaleButtonPressed() {
        if isMaleCheck == false {
            isMaleCheck = true
            RadioButtonMale.isSelected = true
            isFemaleCheck = false
            RadioButtonFemale.isSelected = false
        } else {
            isMaleCheck = false
            RadioButtonMale.isSelected = false
        }
    }
    
    func FemaleButtonPressed() {
        if isFemaleCheck == false{
            isFemaleCheck = true
            RadioButtonFemale.isSelected = true
            isMaleCheck = false
            RadioButtonMale.isSelected = false
        } else {
            isFemaleCheck = false
            RadioButtonFemale.isSelected = false
        }
    }
    
    func TermsAndConditionsButtonPressed() {
        if isTermsAndContionsChecked {
            isTermsAndContionsChecked = false
            RadioButtonTermsAndConditions.isSelected = false
        } else {
            isTermsAndContionsChecked = true
            RadioButtonTermsAndConditions.isSelected = true
        }
    }
    
    func CompanyAccountButtonPressed() {
        if isCompanyAccountChecked {
            isCompanyAccountChecked = false
            RadioButtonCompanyAccount.isSelected = false
            isCompanyAccount(isCompany: false)
        } else {
            isCompanyAccountChecked = true
            RadioButtonCompanyAccount.isSelected = true
            isCompanyAccount(isCompany: true)
        }
    }
    
    func isCompanyAccount(isCompany: Bool) {
        if isCompany {
            UIView.animate(withDuration: 0.5, animations: {
                self.CompanyAccountCardViewHeightConstraint.constant = 180
                self.CompanyAccountTextsView.isHidden = false
            })
        } else {
            UIView.animate(withDuration: 0.5, animations: {
                self.CompanyAccountCardViewHeightConstraint.constant = 90
                self.CompanyAccountTextsView.isHidden = true
            })
        }
    }

}

extension ProfileViewController {
    @IBAction func NextBarButtonItemTapped(_ sender: Any) {
        guard let vc = UIStoryboard(name:"Main", bundle:nil).instantiateViewController(withIdentifier: "LocationViewController") as? LocationViewController else {
            assert(false, "Could not instantiate view controller with identifier of type LocationViewController")
            return
        }
        self.navigationController?.pushViewController(vc, animated:true)
    }
}
