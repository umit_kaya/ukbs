//
//  EzplorColors.swift
//  Ezplor Local Expert
//
//  Created by umit on 30/09/2017.
//  Copyright © 2017 Ezplor. All rights reserved.
//

class CommonEnums: NSObject {
    static let countries = "Countries"
    static let languages = "Languages"
    static let currencies = "Currencies"
    static let cities = "Cities"
    static let codes = "CountryCodes"
    static let statuses = "ExperienceStatuses"
    static let categories = "Categories"
    
    static let Cambodia = "Cambodia"
    static let India = "India"
    static let Indonesia = "Indonesia"
    static let Malaysia = "Malaysia"
    static let Philippines = "Philippines"
    static let Singapore = "Singapore"
    static let Thailand = "Thailand"
    static let Vietnam = "Vietnam"
}
    
func CustomPopupHidePickupButton(type: String) -> Bool {
    switch type {
    case CommonEnums.countries:
        return true
    case CommonEnums.languages:
        return false
    case CommonEnums.currencies:
        return true
    case CommonEnums.cities:
        return true
    case CommonEnums.statuses:
        return true
    case CommonEnums.categories:
        return true
    default:
        return false
    }
}

func CustomPopupMultipleSelectionAllowed(type: String) -> Bool {
    switch type {
    case CommonEnums.countries:
        return false
    case CommonEnums.languages:
        return true
    case CommonEnums.currencies:
        return false
    case CommonEnums.cities:
        return false
    case CommonEnums.statuses:
        return false
    case CommonEnums.categories:
        return false
    default:
        return false
    }
}

func CitiesInCountry(country: String) -> String {
    return "Cities-\(country)"
}

func PopupTypeByTag(tagNo: Int) -> String {
    switch tagNo {
    case 20:
        return CommonEnums.statuses
    case 30:
        return CommonEnums.codes
    case 40:
        return CommonEnums.currencies
    case 50:
        return CommonEnums.countries
    case 60:
        return CommonEnums.cities
    case 70:
        return CommonEnums.languages        
    case 0:
        assert(false)
    default:
        return ""
    }
}

