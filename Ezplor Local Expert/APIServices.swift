//
//  APIServices.swift
//  Ezplor Local Expert
//
//  Created by umit on 13/11/2017.
//  Copyright © 2017 Ezplor. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

final class APIServices {
    
    private init() {
        Alamofire.URLSessionConfiguration.default.requestCachePolicy = .reloadIgnoringLocalCacheData
    }
    
    static let shared = APIServices()
    
    func login(email: String, password: String, completion: @escaping (_ success : Bool, _ errorMessage : String, _ response : JSON) -> ()) {
        
        let parameters: Parameters = ["name": email, "pass": password, "token": LoginToken]
        
        let headers: HTTPHeaders = [
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        
        Alamofire.request(LoginApi, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: headers).responseJSON { response in
            
            self.checkResponse(result: response.result, completion: { (success, message, response) in
                completion(success, message, response)
            })
            
        }
    }
    
    //Validate Api Response
    func checkResponse(result: Result<Any>, completion: @escaping (_ success : Bool, _ errorMessage : String, _ response : JSON) -> ()){
        
        if((result.value) != nil) {
            
            let responseBody = JSON(result.value!)
            let success = responseBody["success"].bool!
            
            if success == true{
                completion(success, "SUCCESS", responseBody)
            }else{
                print("SERVER ERROR \(responseBody)")
                guard let message = responseBody["message"].string else {
                    completion(false, "SERVER ERROR", responseBody)
                    return
                }
                completion(false, message, responseBody)
            }
            
        }else{
            print("null response")
            completion(false, "Conection Problem, PLease Try Again", JSON.null)
        }
        
    }
    
}
