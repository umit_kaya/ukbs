//
//  ExperiencesListTableViewCell.swift
//  Ezplor Local Expert
//
//  Created by umit on 05/10/2017.
//  Copyright © 2017 Ezplor. All rights reserved.
//

import UIKit

class ExperiencesListTableViewCell: UITableViewCell {

    @IBOutlet weak var idView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        idView.layer.cornerRadius = idView.frame.size.height / 2
        idView.clipsToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
