//
//  ParentViewController.swift
//  Ezplor Local Expert
//
//  Created by Soheil Nikbin on 07/08/2017.
//  Copyright © 2017 Ezplor. All rights reserved.
//

import UIKit
import Foundation
import XLPagerTabStrip
import JVFloatLabeledTextField
import UIColor_Hex_Swift

class ParentViewController: ButtonBarPagerTabStripViewController {

    
    @IBOutlet weak var facebookButton: UIButton!
    @IBOutlet weak var googleButton: UIButton!
    
    let purpleInspireColor = UIColor(red:0.13, green:0.03, blue:0.25, alpha:1.0)
     let transparent = UIColor(red:0.13, green:0.03, blue:0.25, alpha:0.0)
    
    override func viewDidLoad() {
        // change selected bar color
        settings.style.buttonBarBackgroundColor = transparent
        settings.style.buttonBarItemBackgroundColor = transparent
        settings.style.selectedBarBackgroundColor = UIColor("#ffffff")
        settings.style.buttonBarItemFont = .boldSystemFont(ofSize: 14)
        settings.style.selectedBarHeight = 2.0
        settings.style.buttonBarMinimumLineSpacing = 0
        settings.style.buttonBarItemTitleColor = UIColor("#ffffff")
        settings.style.buttonBarItemsShouldFillAvailiableWidth = true
        settings.style.buttonBarLeftContentInset = 0
        settings.style.buttonBarRightContentInset = 0
        
        changeCurrentIndexProgressive = { [weak self] (oldCell: ButtonBarViewCell?, newCell: ButtonBarViewCell?, progressPercentage: CGFloat, changeCurrentIndex: Bool, animated: Bool) -> Void in
            guard changeCurrentIndex == true else { return }
            oldCell?.label.textColor = UIColor("#cccccc")
            newCell?.label.textColor = UIColor("#ffffff")
        }
        
        
//        facebookButton.layer.cornerRadius = 5;
//        facebookButton.layer.borderWidth = 3;
//        facebookButton.layer.borderColor = UIColor("#d81377").cgColor
//        facebookButton.layer.backgroundColor = UIColor("#d81377").cgColor
//        facebookButton.clipsToBounds = true
//        facebookButton.addTarget(self, action: #selector(thumbsUpButtonPressed), for: .touchUpInside)
//        view.addSubview(facebookButton)
//        
//        googleButton.layer.cornerRadius = 5;
//        googleButton.layer.borderWidth = 3;
//        googleButton.layer.borderColor = UIColor("#d81377").cgColor
//        googleButton.layer.backgroundColor = UIColor("#d81377").cgColor
//        googleButton.clipsToBounds = true
//        googleButton.addTarget(self, action: #selector(thumbsUpButtonPressed), for: .touchUpInside)
//        view.addSubview(googleButton)
        
        super.viewDidLoad()
        
        
    }
    
    func thumbsUpButtonPressed() {
        
    }
    
    override func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
        let child_1 = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "login")
        let child_2 = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "signup")
        return [child_1, child_2]
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
