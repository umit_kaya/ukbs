//
//  MealsAvailableTableViewCell.swift
//  Ezplor Local Expert
//
//  Created by umit on 15/12/2017.
//  Copyright © 2017 Ezplor. All rights reserved.
//

import UIKit

class MealsAvailableTableViewCell: UITableViewCell {

    @IBOutlet weak var mealDetailsView: UIView!
    @IBOutlet weak var radioButtonYes: RadioButton!
    @IBOutlet weak var radioButtonNo: RadioButton!
    @IBOutlet weak var radioButtonVeggy: RadioButton!
    @IBOutlet weak var radiButtonNonveggy: RadioButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
