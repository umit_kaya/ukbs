//
//  CreateInfoDescTableViewCell.swift
//  Ezplor Local Expert
//
//  Created by umit on 13/10/2017.
//  Copyright © 2017 Ezplor. All rights reserved.
//

import UIKit

class CreateInfoDescTableViewCell: UITableViewCell {

    @IBOutlet weak var descriptionTextField: UITextView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        descriptionTextField.layer.cornerRadius = 5
        descriptionTextField.layer.borderWidth = 1.0
        descriptionTextField.layer.borderColor = UIColor.lightGray.cgColor
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
