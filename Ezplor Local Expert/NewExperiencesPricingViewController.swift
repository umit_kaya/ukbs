//
//  NewExperiencesPricingViewController.swift
//  Ezplor Local Expert
//
//  Created by umit on 10/10/2017.
//  Copyright © 2017 Ezplor. All rights reserved.
//

import UIKit
import XLPagerTabStrip

class NewExperiencesPricingViewController: UIViewController, IndicatorInfoProvider {
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
    }
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: " Pricing ")
    }
    
    func setupUI() {
        
        self.tableView.dataSource = self
        self.tableView.delegate = self
        
        tableView.register(UINib(nibName: "PricingExplanationTableViewCell", bundle: nil), forCellReuseIdentifier: "pricingDescCell")
        tableView.register(UINib(nibName: "PricingPickerTableViewCell", bundle: nil), forCellReuseIdentifier: "pricingPickerCell")
        tableView.register(UINib(nibName: "PricingTypeRadioTableViewCell", bundle: nil), forCellReuseIdentifier: "bookingTypeCell")
    }
    
}

extension NewExperiencesPricingViewController {
    @IBAction func backBarButtonItemTapped(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    @IBAction func nextBarButtonItemTapped(_ sender: Any) {
        guard let vc = UIStoryboard(name:"NewExperience", bundle:nil).instantiateViewController(withIdentifier: "NewExperiencesPhotosViewController") as? NewExperiencesPhotosViewController else {
            assert(false, "Could not instantiate view controller with identifier of type ContactViewController")
            return
        }
        self.navigationController?.pushViewController(vc, animated:true)
    }
}

extension NewExperiencesPricingViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        switch indexPath.row {
        case 0:
            return 290
        case 1:
            return 86
        case 2:
            return 86
        case 3:
            return 110
        case 4:
            return 86
        default:
            return 86
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "pricingDescCell", for: indexPath) as? PricingExplanationTableViewCell
            
            cell?.selectionStyle = .none
            
            
            return cell!
        case 1:
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "pricingPickerCell", for: indexPath) as? PricingPickerTableViewCell
            
            cell?.selectionStyle = .none
            
            cell?.pickerTitle.text = "Minimum Person"
            cell?.pickerSubtitle.text = "Please Select"
            
            return cell!
        case 2:
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "pricingPickerCell", for: indexPath) as? PricingPickerTableViewCell
            
            cell?.selectionStyle = .none
            
            cell?.pickerTitle.text = "Maximum Person"
            cell?.pickerSubtitle.text = "Please Select"
            
            return cell!
        case 3:
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "bookingTypeCell", for: indexPath) as? PricingTypeRadioTableViewCell
            
            cell?.selectionStyle = .none
            
            return cell!
        case 4:
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "pricingPickerCell", for: indexPath) as? PricingPickerTableViewCell
            
            cell?.selectionStyle = .none
            
            cell?.pickerTitle.text = "I ACCEPT BOOKINGS UP TO IN ADVANCE"
            cell?.pickerSubtitle.text = "Please Select"
            
            return cell!
        default:
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "pricingPickerCell", for: indexPath) as? PricingPickerTableViewCell
            
            cell?.selectionStyle = .none
            
            cell?.pickerTitle.text = "Maximum Person"
            cell?.pickerSubtitle.text = "Please Select"

            return cell!
        }
    }
}
