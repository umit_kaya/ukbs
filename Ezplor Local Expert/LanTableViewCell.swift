//
//  LanTableViewCell.swift
//  Ezplor Local Expert
//
//  Created by Soheil Nikbin on 09/08/2017.
//  Copyright © 2017 Ezplor. All rights reserved.
//

import UIKit

class LanTableViewCell: UITableViewCell {
    
    @IBOutlet weak var Title: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
