//
//  NewItenaryPickViewTableViewCell.swift
//  Ezplor Local Expert
//
//  Created by umit on 15/12/2017.
//  Copyright © 2017 Ezplor. All rights reserved.
//

import UIKit

class NewItenaryPickViewTableViewCell: UITableViewCell {

    @IBOutlet weak var pickerMainTitle: UILabel!
    @IBOutlet weak var pickerTitle: UILabel!
    @IBOutlet weak var pickerView: CardView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
