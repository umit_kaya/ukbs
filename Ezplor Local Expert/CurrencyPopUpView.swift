//
//  CurrencyPopUpView.swift
//  Ezplor Local Expert
//
//  Created by Soheil Nikbin on 09/08/2017.
//  Copyright © 2017 Ezplor. All rights reserved.
//

import UIKit

class CurrencyPopUpView: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet
    var tableView: UITableView!
    var itemsDictionary: [String:String] = [:]
    
    var selectedData = ""
    var selectedCells = [NSIndexPath]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.7)
        self.showAnimate()
        
        if let path = Bundle.main.path(forResource: "PropertyList", ofType: "plist") {
            if let propertiesDictionary = NSDictionary(contentsOfFile: path) as? [String: Any] {
                let properties = propertiesDictionary[CommonEnums.currencies] as! [String: String]
                for (key,value) in properties {
                    print(key, value)
                    itemsDictionary[key] = value
                }
            }
        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return itemsDictionary.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as? LanTableViewCell
        cell?.Title.text = Array(self.itemsDictionary.values)[indexPath.row]
        cell?.accessoryType = cell?.Title.text == selectedData ? .checkmark : .none
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedCell = tableView.cellForRow(at: indexPath as IndexPath)
        
        selectedCells.append(indexPath as NSIndexPath)
        
        if selectedCell?.accessoryType == .checkmark {
            selectedData.removeAll()
            selectedCell?.accessoryType = .none
            selectedData = Array(self.itemsDictionary.values)[indexPath.row]
            selectedCells = selectedCells.filter {$0 as IndexPath != indexPath}
            
        } else {
            selectedData = Array(self.itemsDictionary.values)[indexPath.row]
            selectedCell?.accessoryType = .checkmark
            CustomPopupMultipleSelectionAllowed(type: CommonEnums.currencies) ? nil : removeAnimate()
        }
        print("items \(selectedData)")
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func cancel(_ sender: Any) {
        self.removeAnimate()
    }
    @IBAction func pick(_ sender: Any) {
        self.removeAnimate()
    }
    
    func showAnimate()
    {
        self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0.0;
        UIView.animate(withDuration: 0.25, animations: {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        });
    }
    
    func removeAnimate()
    {
        UIView.animate(withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view.alpha = 0.0;
        }, completion:{(finished : Bool)  in
            if (finished)
            {
                self.view.removeFromSuperview()
            }
        });
    }
}

