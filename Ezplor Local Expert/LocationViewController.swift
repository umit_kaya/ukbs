//
//  NewsTableViewController.swift
//  SidebarMenu
//
//  Created by Simon Ng on 2/2/15.
//  Copyright (c) 2015 AppCoda. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class LocationViewController: UIViewController, CLLocationManagerDelegate, MKMapViewDelegate {
    @IBOutlet var menuButton:UIBarButtonItem!
    @IBOutlet weak var pickCountry: CardView!
    @IBOutlet weak var selectCountryLabel: UILabel!
    @IBOutlet weak var selectCityLabel: UILabel!
    @IBOutlet weak var pickCity: CardView!
    @IBOutlet weak var MapViewContainerView: UIView!
    @IBOutlet weak var BackButton: ActionButton!
    var MapView = MapViewController()
    var popupType = ""
    var locationManager = CLLocationManager()
    var fullAddress = "Kuala Lumpur"
    var state = ""
    var selectedCity = ""
    var customPopupVC = CustomPopupViewController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configure()
    }
    
    func configure() {
        if revealViewController() != nil {
            menuButton.target = revealViewController()
            menuButton.action = #selector(SWRevealViewController.revealToggle(_:))
            revealViewController().rightViewRevealWidth = 150
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        // Configure Notificatoin for country/city Selection
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(countrySelected(_:)), name: .countrySelected, object: nil)
        notificationCenter.addObserver(self, selector: #selector(citySelected(_:)), name: .citySelected, object: nil)

        let tapCountry = UITapGestureRecognizer(target: self, action: #selector(showPopup))
        tapCountry.delegate = self as? UIGestureRecognizerDelegate
        pickCountry.addGestureRecognizer(tapCountry)
        
        let tapCity = UITapGestureRecognizer(target: self, action: #selector(showPopup))
        tapCity.delegate = self as? UIGestureRecognizerDelegate
        pickCity.addGestureRecognizer(tapCity)
    }
    
    deinit {
        let notificationCenter = NotificationCenter.default
        notificationCenter.removeObserver(self, name: .countrySelected, object: nil)
        notificationCenter.removeObserver(self, name: .citySelected, object: nil)
    }
    
    func showPopup(sender: UITapGestureRecognizer? = nil) {
        if let popupType: String = PopupTypeByTag(tagNo: (sender?.view?.tag)!) {
            if popupType == "Countries" {
             self.resetSelectedCity()
            }
            let CustomPopUp = UIStoryboard(name: "CustomPopupStoryboard", bundle: nil).instantiateViewController(withIdentifier: "custom_popup") as! CustomPopupViewController
            CustomPopUp.popupType = popupType
            CustomPopUp.isACity = popupType == "Cities" ? true : false
            CustomPopUp.inCountry = self.selectCountryLabel.text!
            self.addChildViewController(CustomPopUp)
            CustomPopUp.view.frame = self.view.frame
            self.view.addSubview(CustomPopUp.view)
            CustomPopUp.didMove(toParentViewController: self)
        } else {
            assert(false, "Add TAG for the view!")
        }
        customPopupVC.delegate = self
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    }
    
    func loadMapView(with state:String) {
        MapView = UIStoryboard(name: "MapViewStoryboard", bundle: nil).instantiateViewController(withIdentifier: "map_view") as! MapViewController
        if !selectedCity.isEmpty {
            MapView.fullAddress = self.selectedCity
            MapView.state = self.selectedCity
        }
        else {
            MapView.fullAddress = self.state
            MapView.state = self.state
        }
        if MapViewContainerView == nil {
            self.MapViewContainerView.frame = CGRect (x: 0, y: 0, width: 359.0, height: 150.0)
        }
        MapView.view.frame = MapViewContainerView.bounds
        self.MapViewContainerView.addSubview(MapView.view)
        addChildViewController(MapView)
        MapView.didMove(toParentViewController: self)
    }
    
    func backButtonTapped(sender: UITapGestureRecognizer? = nil) {
        _ = navigationController?.popViewController(animated: true)
    }

}

extension LocationViewController {
    @IBAction func nextBarButtonItemTapped(_ sender: Any) {
        guard let vc = UIStoryboard(name:"Main", bundle:nil).instantiateViewController(withIdentifier: "ContactViewController") as? ContactViewController else {
            assert(false, "Could not instantiate view controller with identifier of type ContactViewController")
            return
        }
        self.navigationController?.pushViewController(vc, animated:true)
    }
    @objc func countrySelected(_ notification: Notification) {
        if let countrySelected = notification.userInfo?["countrySelected"] as? String {
            self.state = countrySelected
            self.loadMapView(with: self.state)
            self.selectCountryLabel.text = self.state
        }
    }
    @objc func citySelected(_ notification: Notification) {
        if let citySelected = notification.userInfo?["citySelected"] as? String {
            self.selectedCity = citySelected
            self.loadMapView(with: citySelected)
            self.selectCityLabel.text = self.selectedCity
        }
    }
    @objc func resetSelectedCity() {
        self.selectedCity = ""
        self.selectCityLabel.text = ""
    }
}
extension LocationViewController: CustomPopupViewControllerDelegate {
    func didFinishTask(sender: CustomPopupViewController) {
        // TODO
        print("done")
    }
}

