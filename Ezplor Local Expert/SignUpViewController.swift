//
//  SignUpViewController.swift
//  Ezplor Local Expert
//
//  Created by Soheil Nikbin on 07/08/2017.
//  Copyright © 2017 Ezplor. All rights reserved.
//

import UIKit
import Foundation
import XLPagerTabStrip
import JVFloatLabeledTextField
import UIColor_Hex_Swift

class SignUpViewController: UIViewController, IndicatorInfoProvider, UITextFieldDelegate  {
    @IBOutlet weak var SignUpButton: UIButton!

    @IBOutlet weak var backgroundScrollView: UIScrollView!
    
    @IBOutlet weak var GivenName: JVFloatLabeledTextField!
    @IBOutlet weak var FamilyName: JVFloatLabeledTextField!
    @IBOutlet weak var EmailAddress: JVFloatLabeledTextField!
    @IBOutlet weak var Password: JVFloatLabeledTextField!
    @IBOutlet weak var ConfirmPassword: JVFloatLabeledTextField!
    
    var ActiveOpenKeyboard: Bool = true
    var KeyboardIsOn: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        GivenName.delegate = self
        GivenName.tag = 0
        FamilyName.delegate = self
        FamilyName.tag = 1
        EmailAddress.delegate = self
        EmailAddress.tag = 2
        Password.delegate = self
        Password.tag = 3
        ConfirmPassword.delegate = self
        ConfirmPassword.tag = 4

    }
    
    override func viewWillAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        // Try to find next responder
        if let nextField = textField.superview?.viewWithTag(textField.tag + 1) as? UITextField {
            nextField.becomeFirstResponder()
        } else {
            // Not found, so remove keyboard.
            textField.resignFirstResponder()
            ActiveOpenKeyboard = true
            self.view.endEditing(true)
        }
        // Do not add a line break
        return false
    }
    
    func keyboardWillShow(notification:NSNotification) {
        adjustingHeight(show: true, notification: notification)
        KeyboardIsOn = true
    }
    
    func keyboardWillHide(notification:NSNotification) {
        KeyboardIsOn = false
        ActiveOpenKeyboard = true
        adjustingHeight(show: false, notification: notification)
        
    }
    
    func adjustingHeight(show:Bool, notification:NSNotification) {
        
        if(ActiveOpenKeyboard == true){
            ActiveOpenKeyboard = false
            
            let userInfo = notification.userInfo!
            
            let keyboardFrame = (userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
            
            let changeInHeight = (keyboardFrame.height + 40) * (show ? 1 : -1)
            if show == false{
                backgroundScrollView.contentInset.bottom = 0
                backgroundScrollView.scrollIndicatorInsets.bottom = 0
            }else{
                backgroundScrollView.contentInset.bottom += changeInHeight
                backgroundScrollView.scrollIndicatorInsets.bottom += changeInHeight
            }
        }
    }
    
    func textFieldShouldReturnScroll(textField: JVFloatLabeledTextField) -> Bool {
        self.view.endEditing(false)
        return true
    }
    
    func thumbsUpButtonPressed() {
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: "SIGNUP")
    }
}
