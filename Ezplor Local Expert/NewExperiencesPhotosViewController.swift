//
//  NewExperiencesPhotosViewController.swift
//  Ezplor Local Expert
//
//  Created by umit on 10/10/2017.
//  Copyright © 2017 Ezplor. All rights reserved.
//

import UIKit
import XLPagerTabStrip

class NewExperiencesPhotosViewController: UIViewController, IndicatorInfoProvider {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: " Photos ")
    }


}

extension NewExperiencesPhotosViewController {
    @IBAction func backBarButtonItemTapped(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    @IBAction func nextBarButtonItemTapped(_ sender: Any) {
        guard let vc = UIStoryboard(name:"NewExperience", bundle:nil).instantiateViewController(withIdentifier: "NewExperiencesScheduleViewController") as? NewExperiencesScheduleViewController else {
            assert(false, "Could not instantiate view controller with identifier of type ContactViewController")
            return
        }
        self.navigationController?.pushViewController(vc, animated:true)
    }
}
