//
//  MapViewController.swift
//  Ezplor Local Expert
//
//  Created by umit on 01/10/2017.
//  Copyright © 2017 Ezplor. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class MapViewController: UIViewController, CLLocationManagerDelegate, MKMapViewDelegate {
    
    var locationManager = CLLocationManager()
    @IBOutlet weak var MapView: MKMapView!
    var fullAddress = ""
    var state = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        MapView.mapType = MKMapType.standard
        MKPinAnnotationView.greenPinColor()

        let geocoder = CLGeocoder()
        geocoder.geocodeAddressString(fullAddress) {
            placemarks, error in

            let placemark = placemarks?.first
            let lat = placemark?.location?.coordinate.latitude
            let lon = placemark?.location?.coordinate.longitude

            let location = CLLocationCoordinate2D(latitude: lat!,longitude: lon!)
            let span = MKCoordinateSpanMake(0.05, 0.05)
            let region = MKCoordinateRegion(center: location, span: span)
            self.MapView.setRegion(region, animated: true)

            let annotation = MKPointAnnotation()
            annotation.coordinate = location
            annotation.title = self.state
            self.MapView.addAnnotation(annotation)
        }
    }
    
}
