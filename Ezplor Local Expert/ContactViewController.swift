//
//  PhotoViewController.swift
//  SidebarMenu
//
//  Created by Simon Ng on 2/2/15.
//  Copyright (c) 2015 AppCoda. All rights reserved.
//

import UIKit

class ContactViewController: UIViewController {
    @IBOutlet weak var menuButton:UIBarButtonItem!
    @IBOutlet weak var pickCountryCode: CardView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if revealViewController() != nil {
            menuButton.target = revealViewController()
            menuButton.action = #selector(SWRevealViewController.revealToggle(_:))
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        
        let tapCountryCode = UITapGestureRecognizer(target: self, action: #selector(showPopup))
        tapCountryCode.delegate = self as? UIGestureRecognizerDelegate
        pickCountryCode.addGestureRecognizer(tapCountryCode)
        
    }

    func showPopup(sender: UITapGestureRecognizer? = nil) {
        if let popupType: String = PopupTypeByTag(tagNo: (sender?.view?.tag)!) {
            let CustomPopUp = UIStoryboard(name: "CustomPopupStoryboard", bundle: nil).instantiateViewController(withIdentifier: "custom_popup") as! CustomPopupViewController
            CustomPopUp.popupType = popupType
            self.addChildViewController(CustomPopUp)
            CustomPopUp.view.frame = self.view.frame
            self.view.addSubview(CustomPopUp.view)
            CustomPopUp.didMove(toParentViewController: self)
        } else {
            assert(false, "Add TAG for the view!")
        }
    }
    
    func nextButtonTapped(sender: UITapGestureRecognizer? = nil) {
        guard let vc = UIStoryboard(name:"Main", bundle:nil).instantiateViewController(withIdentifier: "ContactViewController") as? ContactViewController else {
            assert(false, "Could not instantiate view controller with identifier of type ContactViewController")
            return
        }
        self.navigationController?.pushViewController(vc, animated:true)
    }

}

extension ContactViewController {
    @IBAction func nextBarButtonItemTapped(_ sender: Any) {
        guard let vc = UIStoryboard(name:"Main", bundle:nil).instantiateViewController(withIdentifier: "PaymentViewController") as? PaymentViewController else {
            assert(false, "Could not instantiate view controller with identifier of type ContactViewController")
            return
        }
        self.navigationController?.pushViewController(vc, animated:true)
    }
}
