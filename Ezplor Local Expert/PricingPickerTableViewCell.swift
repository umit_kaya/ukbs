//
//  PricingPickerTableViewCell.swift
//  Ezplor Local Expert
//
//  Created by umit on 18/12/2017.
//  Copyright © 2017 Ezplor. All rights reserved.
//

import UIKit

class PricingPickerTableViewCell: UITableViewCell {

    @IBOutlet weak var pickerView: CardView!
    @IBOutlet weak var pickerTitle: UILabel!
    @IBOutlet weak var pickerSubtitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
