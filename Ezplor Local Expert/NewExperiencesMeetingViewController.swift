//
//  NewExperiencesMeetingViewController.swift
//  Ezplor Local Expert
//
//  Created by umit on 10/10/2017.
//  Copyright © 2017 Ezplor. All rights reserved.
//

import UIKit
import XLPagerTabStrip

class NewExperiencesMeetingViewController: UIViewController,IndicatorInfoProvider {
    
    @IBOutlet weak var tableView: UITableView!
    
    var guestSelected = false
    var publicSelected = false
    var localSelected = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
    }
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: " Meeting ")
    }
    
    func setupUI() {
        self.tableView.dataSource = self
        self.tableView.delegate = self
        
        tableView.register(UINib(nibName: "MeetupPlaceTableViewCell", bundle: nil), forCellReuseIdentifier: "guestsCell")
        tableView.register(UINib(nibName: "MeetingAdressTableViewCell", bundle: nil), forCellReuseIdentifier: "mapAddressCell")
    }
    
}

extension NewExperiencesMeetingViewController {
    @IBAction func backBarButtonItemTapped(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    @IBAction func nextBarButtonItemTapped(_ sender: Any) {
        guard let vc = UIStoryboard(name:"NewExperience", bundle:nil).instantiateViewController(withIdentifier: "NewExperiencesPricingViewController") as? NewExperiencesPricingViewController else {
            assert(false, "Could not instantiate view controller with identifier of type ContactViewController")
            return
        }
        self.navigationController?.pushViewController(vc, animated:true)
    }
}

extension NewExperiencesMeetingViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        switch indexPath.row {
        case 0:
           return 180
        default:
            return 340
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "guestsCell", for: indexPath) as? MeetupPlaceTableViewCell
            
            cell?.selectionStyle = .none
            
            cell?.radioLocal.addTarget(self, action: #selector(localPressed), for: .touchUpInside)
            cell?.radioPublic.addTarget(self, action: #selector(publicPressed), for: .touchUpInside)
            cell?.radioGuestPickup.addTarget(self, action: #selector(guestPressed), for: .touchUpInside)
            
            return cell!
        default:
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "mapAddressCell", for: indexPath) as? MeetingAdressTableViewCell
            cell?.selectionStyle = .none

            return cell!
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    
    func localPressed() {
        let indexPath = IndexPath(row: 0, section: 0)
        let cell = tableView.cellForRow(at: indexPath) as! MeetupPlaceTableViewCell
        
        if localSelected {
            localSelected = false
            publicSelected = false
            guestSelected = false
            
            cell.radioGuestPickup.isSelected = false
            cell.radioPublic.isSelected = false
            cell.radioLocal.isSelected = false
        } else {
            localSelected = true
            publicSelected = false
            guestSelected = false
            
            cell.radioGuestPickup.isSelected = false
            cell.radioPublic.isSelected = false
            cell.radioLocal.isSelected = true
        }
    }
    
    func publicPressed() {
        let indexPath = IndexPath(row: 0, section: 0)
        let cell = tableView.cellForRow(at: indexPath) as! MeetupPlaceTableViewCell
        
        if publicSelected {
            localSelected = false
            publicSelected = false
            guestSelected = false
            
            cell.radioGuestPickup.isSelected = false
            cell.radioPublic.isSelected = false
            cell.radioLocal.isSelected = false
        } else {
            localSelected = false
            publicSelected = true
            guestSelected = false
            
            cell.radioGuestPickup.isSelected = false
            cell.radioPublic.isSelected = true
            cell.radioLocal.isSelected = false
        }
    }
    
    func guestPressed() {
        let indexPath = IndexPath(row: 0, section: 0)
        let cell = tableView.cellForRow(at: indexPath) as! MeetupPlaceTableViewCell
        
        if guestSelected {
            localSelected = false
            publicSelected = false
            guestSelected = false
            
            cell.radioGuestPickup.isSelected = false
            cell.radioPublic.isSelected = false
            cell.radioLocal.isSelected = false
        } else {
            localSelected = false
            publicSelected = false
            guestSelected = true
            
            cell.radioGuestPickup.isSelected = true
            cell.radioPublic.isSelected = false
            cell.radioLocal.isSelected = false
        }
    }
}
